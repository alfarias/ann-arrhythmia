"""" 

                  Trabalho de Computação Evolucionária

Código Referente ao Trabalho Final: Definição de Parâmetros de uma RNA Usando Algoritmo Genético

"""

import random
from random import randint 
import numpy as np
import csv
from sklearn.neural_network import MLPClassifier
# import plots
import warnings # Pra ignorar os warinigs causados pela função de máximo e minimo
warnings.filterwarnings("ignore")


class GetConfigFile(object): # Classe para ler o arquivo de configuração de parâmetros
    def __init__(self, file):
        self.file = file
        
    def read_file(self): # Função que realiza a leitura do arquivo de texto linha por linha
        with open(self.file) as f:
            content = f.read().splitlines()
        return content

class ReadFile(object): # Classe para a leitura da Base de Dados no Formato .csv

    def __init__(self, csv_size, total_rows):
        self.csv_size = csv_size
        self.total_rows = total_rows

    def read_single_row(self, file):
        with open(file, 'r') as arch:
            reader = csv.reader(arch, delimiter=',')
            file_data = []
            for p in range(0, self.csv_size):
                file_data.append(0)
            for row in reader:
                var_index = 0
                for value in row:
                    file_data[var_index] = float(value)
                    var_index += 1
        return file_data

    def read_multiple_rows(self, file):
        with open(file, 'r') as arch:
            index_row = 0
            reader = csv.reader(arch, delimiter=',')
            file_data = np.zeros((self.total_rows, self.csv_size))
            for row in reader:
                var_index = 0
                if row != []:
                    for value in row:
                        file_data[index_row][var_index] = float(value)
                        var_index += 1
                    index_row +=1
        return file_data

class Chromosome(object): # Classe onde o Cromossomo é gerado.
    def __init__(self, length, max, min):
        self.length = length
        self.max = max
        self.min = min
        
    def create_chromosome(self):
        return [round(random.uniform(self.min,self.max),5) for x in range(self.length)] # Cria o Cromossomo com no máximo 5 casas decimais

class GeneratePopulation(object): # Classe onde a população é gerada.
    def __init__(self, count):
        self.count = count
        
    def create_population(self, length, max, min):
        chromosome = Chromosome(length, max, min) # Instaciamento
        return [chromosome.create_chromosome() for x in range(self.count)]
        
class Evaluate(object):
# A classe avaliador é a interface do AG com o problema que está sendo otimizado. 
# Nesta classe o cromossomo é decodificado para gerar o individuo, do qual é calculado o fitness.
    def __init__(self, activation, solver,num_samples):
        self.activation = activation
        self.solver = solver
        self.num_samples = num_samples

    def decode(self, chromosome): # Decodifica o Cromossomo
        individual = []
        for i in range(len(chromosome)): # Necessário criar como vetor, pois o numpy acaba não tornando possível fazer a iteração
            individual.append(i)

        individual[0] = int((chromosome[0])*(30-2)+(2))
        if chromosome[1] < 0.25:
            individual[1] = self.activation[0]
        elif chromosome[1] >= 0.25 and chromosome[1] < 0.50:
            individual[1] = self.activation[1]
        elif chromosome[1] >= 0.50 and chromosome[1] < 0.75:
            individual[1] = self.activation[2]
        elif chromosome[1] >= 0.75:
            individual[1] = self.activation[3]
        else:
            pass
        if chromosome[2] < 0.33:
            individual[2] = self.solver[0]
        elif chromosome[2] >= 0.33 and chromosome[2] < 0.66:
            individual[2] = self.solver[1]
        elif chromosome[2] >= 0.66:
            individual[2] = self.solver[2]
        return individual
    
    def calculate_fitness(self, chromosome, input_train, target_train): # Calcula o Fitness
        chromosome_decoded = self.decode(chromosome) # Decodifica o Cromossomo
#        print(chromosome_decoded)
        mlp = MLPClassifier(hidden_layer_sizes=chromosome_decoded[0], activation = chromosome_decoded[1],solver = chromosome_decoded[2],max_iter=120)
        mlp.fit(input_train,target_train)
        predictions_train = mlp.predict(input_train)
        error_train = np.abs(target_train - np.reshape(predictions_train,(self.num_samples,1)))
        error_sq_train = error_train**2
        sqrt_error_train = (np.sum(error_sq_train))/2 # Erro Quadrático
        sqrt_mean_error_train = (sqrt_error_train)/len(input_train) # Erro Quadrático médio
        return (1/(sqrt_mean_error_train+0.1))

class GeneticAlgorithm(object): # Classe onde há a implementação dos operadores genéticos.
# Deve usar cruzamento aritmético e mutação Gaussiana.
# A seleção deve ser por torneio.
# A troca da população será geracional.   
    
    def __init__(self, population, fitness, population_count):
        self.population = population
        self.fitness = fitness
        self.population_count = population_count
    
    def selection(self, ring_size): # Seleção por Torneio.
        selected_winners = []
        for x in range(len(self.population)):
            selected = [randint(0,len(self.population) - 1) for i in range(ring_size)]
            aux = self.fitness[selected[0]]
            index = selected[0]
            for i in range(1,ring_size-1):
                if self.fitness[selected[i]] >= aux:
                    aux = self.fitness[selected[i]]
                    index = selected[i]
            selected_winners.append(self.population[index])
        return selected_winners
    
    def crossover(self, selected_winners, crossover_probability): # Cruzamento Aritmético.
        offspring = []
        i = 0          
        while i < (int(self.population_count/2)):
            chance = random.random()
            alpha = random.random()
            # Coeficiente aleatório de 0 a 1 para o cálculo do cruzamento aritmético
            if chance <= crossover_probability:
                parent_a = np.array(random.choice(selected_winners))
                parent_b = np.array(random.choice(selected_winners))
            else:
                continue
            if parent_a is not parent_b:
                offspring_a = np.round(parent_a + (parent_b - parent_a)*alpha,5) # Cruzamento aritmético
                offspring_b = np.round(parent_b + (parent_a - parent_b)*alpha,5) # Cruzamento aritmético
                i = i+1
            else:
                continue        
            offspring.append(offspring_a.tolist()) 
            offspring.append(offspring_b.tolist())

        return offspring
    
    def mutation(self, offspring, standard_deviation,mutation_probability): # Função para realizar a mutação gaussiana
    # Realiza a mutação gaussiana gene a gene em cada cromossomo da população
        mutated = [[np.absolute(random.gauss(gene, standard_deviation)) if random.random() <= mutation_probability else gene for gene in sublst] for sublst in offspring]  
        new_mut = [[1-(abs(gene-1)) if gene > 1 else gene for gene in sublst] for sublst in mutated]
        new_mut_round = np.round(new_mut,5)
        new_mut_final = new_mut_round.tolist()
        return new_mut_final
    
    def get_best_fitness(self, offspring, offspring_fitness): # Pega o melhor fitness da população
        best_offspring_fitness = offspring_fitness[0]
        best_offspring = offspring[0]
        for i in range(1, self.population_count):
            if offspring_fitness[i] > best_offspring_fitness:
                best_offspring_fitness = offspring_fitness[i]
                best_offspring = offspring[i]
        return best_offspring_fitness, best_offspring

    def elitism(self, offspring, offspring_fitness, elite, elite_fitness): # Realiza o Elitismo

        best_offspring_fitness, best_offspring = self.get_best_fitness(offspring, offspring_fitness)
        
        if best_offspring_fitness < elite_fitness:
            i = randint(0,population_count-1)
            offspring[i] = elite
            offspring_fitness[i] = elite_fitness
        if best_offspring_fitness > elite_fitness:
            elite_fitness = best_offspring_fitness
            elite = best_offspring
        else:
            pass
        return offspring, offspring_fitness, elite, elite_fitness    

# Leitura dos parâmetros contidos no arquivo txt
data = GetConfigFile('parameters.txt') # Carrega o arquivo de parâmetros
data_file = data.read_file() # Realiza a leitura do arquivo de configuração
population_count = int(data_file[1]) # Tamanho da População - Linha 2
ring_size = int(data_file[3]) # Tamanho do Ring - Linha 4
crossover_probability = float(data_file[5]) # Probabilidade de Crossover - Linha 6
mutation_probability = float(data_file[7]) # Probabilidade de Mutação - Linha 8
standard_deviation = float(data_file[9]) # Desvio-Padrão (Mutação Gaussiana) - Linha 10
generations = int(data_file[11]) # Número de Gerações - Linha 12
attempt = int(data_file[13]) # Número de execuções - Linha 14
# Parâmetros da Rede Neural
activation = data_file[15].split(",")
solver = data_file[17].split(",")
csv_size = 3600 # Total de Amostras da Base de Dados
total_rows = 1 # Total de Linhas do arquivo .csv
data_train = int(data_file[19]) # Número da amostra que deseja ler
num_samples = int(data_file[21]) # Número de amostras do paciente - Linha 22
input_train_data = "C:\\repos\\ann-arrhythmia\\diag\\normalized\\all\\%sm.csv" %(data_train)
target_train_data = "C:\\repos\\ann-arrhythmia\\diag\\standard\\all\\%sm.csv" %(data_train)

read_database = ReadFile(csv_size, total_rows) # Instanciamento para a leitura da Base de Dados
# Conjunto de Treino
input_train = np.array(read_database.read_single_row(input_train_data)) # Entrada Normalizada
input_train = input_train[0:num_samples]
input_train = input_train.reshape(num_samples,1)
target_train = np.array(read_database.read_single_row(target_train_data)) # Saída Desejada
target_train = target_train[0:num_samples]
target_train = target_train.reshape(num_samples,1)
input_train_std = target_train

# Características do Cromossomo
chromosome_length = 3 # Total de Genes do Cromossomo
chromosome_min = 0 # Valor minimo de um gene do Cromossomo
chromosome_max = 1 # Valor máximo de um gene do Cromossomo

# Função Principal
elite_fitness_attempt = []
elite_attempt = []
for i in range(attempt): # Laço iniciar as várias execuções
    population = GeneratePopulation(population_count)
    population_chromosome = population.create_population(chromosome_length,chromosome_max,chromosome_min)
    fitness = Evaluate(activation, solver, num_samples) # Instanciamento da função de Fitness
    population_fitness = []
    predictions_train = []
    for i in range (population_count):
        population_fitness.append(fitness.calculate_fitness(population_chromosome[i], input_train, target_train))

    # Laço para iniciar o AG
    elite_fitness_generation = []
    elite = 0
    elite_fitness = 0
    for i in range(generations):
        ga = GeneticAlgorithm(population_chromosome, population_fitness, population_count) # Instanciamento da Classe GeneticAlgorithm
        selected = ga.selection(ring_size) # Seleção por Torneio
        offspring = ga.crossover(selected, crossover_probability) # Cruzamento Aritmético
        offspring_mutated = ga.mutation(offspring, standard_deviation, mutation_probability) # Mutação Gaussiana
            
        offspring_fitness = []
        for i in range (population_count):
            offspring_fitness.append(fitness.calculate_fitness(offspring_mutated[i], input_train, target_train))

        offspring, offspring_fitness, elite, elite_fitness = ga.elitism(offspring_mutated, offspring_fitness, elite, elite_fitness)
        
#        # Salva a população decodificada
#        population_decoded = []
#        
#        # Decodifica a População
#        for p in range(population_count):
#            population_decoded.append(fitness.decode(offspring_mutated[p]))

                            
        # População final da Geração
        population_chromosome = offspring
        population_fitness = offspring_fitness
        elite_fitness_generation.append(elite_fitness)
    elite_fitness_attempt.append(elite_fitness_generation) # Armazena os fitness de cada geração por execução
    elite_attempt.append(elite) # Melhor cromossomo por execução
        
    
# Salva os Fitness por geração de cada execução em um .csv
best_individual_attempt = []
for k in range(len(elite_attempt)):
    best_individual_attempt.append(fitness.decode(elite_attempt[k]))

file_a = 'best_fitness_attempt_scenario_%s.csv' %(data_train)
with open(file_a,'w') as outfile:
    best_fitness_attempt_data = csv.writer(outfile)
    for row in elite_fitness_attempt:
        best_fitness_attempt_data.writerow(row)

file_b = 'best_individual_attempt_scenario_%s.csv.csv' %(data_train)
with open(file_b,'w') as outfile:
    best_individual_attempt_data = csv.writer(outfile)
    for row in best_individual_attempt:
        best_individual_attempt_data.writerow(row)

# plots.plot()