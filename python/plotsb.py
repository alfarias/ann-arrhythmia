import matplotlib.pyplot as plt
import numpy as np

N = 4
ind = np.arange(N)  # the x locations for the groups
width = 0.35       # the width of the bars

x100m = [8.78048780488, 8.78048780488, 7.82608695652, 8.78048780488, 8.78048780488]
x101m = [10.0, 10.0, 10.0, 10.0,10.0]
x103m = [6.42857142857, 6.42857142857, 6.42857142857, 6.42857142857, 7.05882352941]
x105m = [4.73684210526, 4.44444444444, 4.44444444444, 4.44444444444, 4.73684210526]



fig, ax = plt.subplots()
ax.set_title('Média e Desvio Padrão do Fitness (Acurácia)')
accm = (np.mean(x100m), np.mean(x101m), np.mean(x103m), np.mean(x105m))
accs = (np.std(x100m), np.std(x101m), np.std(x103m), np.std(x105m))
rects1 = ax.bar(ind, accm, width, color='r', yerr=accs)
#rects2 = ax.bar(ind + width, prec, width, color='y')
ax.set_ylabel('Média do Fitness')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('100m','101m','103m','105m'))
#ax.legend((rects1[0], rects2[0]), ('Acurácia', 'Precisão))
plt.show()

x100ma = [20, 7, 16, 12, 5]
x101ma = [15, 24, 10, 18,21]
x103ma = [13, 23, 4, 12, 20]
x105ma = [14, 25, 23, 23, 22]



fig, ax = plt.subplots()
ax.set_title('Desvio Padrão dos Neurônios na Camada Oculta (Precisão)')
precm = (np.std(x100ma), np.std(x101ma), np.std(x103ma), np.std(x105ma))
#precs = (precm[0]/5, np.std(x101ma), np.std(x103ma), np.std(x105ma))
rects1 = ax.bar(ind, precm, width, color='r')
#rects2 = ax.bar(ind + width, prec, width, color='y')
ax.set_ylabel('Desvio Padrão')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels(('100m','101m','103m', '105m'))
#ax.legend((rects1[0], rects2[0]), ('Acurácia', 'Precisão))
plt.show()